
    function mySum(x1, x2){
        return x1 + x2;
    }

    document.getElementById("sum").innerHTML = mySum(8,9);

    function toFahrenheit(c){
        return c * 1.8 + 32;
    }

    document.getElementById("fahrenheit").innerHTML = toFahrenheit(75);


    function calc(){
        let weight = document.getElementById("weight").value;
        let height = document.getElementById("height").value;

        let bmi = weight / ((height / 100) * (height / 100));
        let bmiName;


        if(bmi < 18.5){
            bmiName = "Untergweicht";
        } else if(bmi >= 18.5 && bmi < 25){
            bmiName = "Normalgewicht";
        } else {
            bmiName = "Übergewicht";
        }

        document.getElementById("value").innerHTML = bmiName;
    }


    function hello(){
        window.alert("Hello!");
    }



