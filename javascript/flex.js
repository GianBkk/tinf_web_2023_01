var container, direction, justify, alignBtn, wrap, alignContent;

function findElements(){
  container = document.getElementById("container");
  direction = document.getElementById("direction");
  justify = document.getElementById("justify");
  alignBtn = document.getElementById("align");
  wrap = document.getElementById("wrap");
  alignContent = document.getElementById("alignContent");
}

align = [
  "flex-start",
  "flex-end",
  "center",
  "space-between",
  "space-around",
  "space-evenly",
];
var i = 0;
var j = 0;
var k = 0;
var itemCount = 3;

function changeFlexDirection() {
  findElements();
  if (container.style.flexDirection == "column") {
    container.style.flexDirection = "row";
    direction.innerHTML = "flex-direction: row";
  } else {
    container.style.flexDirection = "column";
    direction.innerHTML = "flex-direction: column";
  }
}

function changeJustifyContent() {
  findElements();
  if (i < align.length - 1) {
    i++;
  } else {
    i = 0;
  }

  console.log(i);

  container.style.justifyContent = align[i];
  justify.innerHTML = `justify-content: ${align[i]}`;
}

function changeAlignItems() {
  findElements();
  if (j < 2) {
    j++;
  } else {
    j = 0;
  }

  container.style.alignItems = align[j];
  alignBtn.innerHTML = `align-items: ${align[j]}`;
}

function changeAlignContent() {
  findElements();
  if (container.style.flexWrap == "wrap") {
    if (k < align.length - 1) {
      k++;
    } else {
      k = 0;
    }
    container.style.alignContent = align[k];
    alignContent.innerHTML = `align-content: ${align[k]}`;
  }
}

function changeWrap() {
  findElements();
  if (container.style.flexWrap == "wrap") {
    container.style.flexWrap = "nowrap";
    container.style.alignContent = "flex-start";
    alignContent.innerHTML = `align-content: flex-start`;
    wrap.innerHTML = `flex-wrap: nowrap`;
  } else {
    container.style.flexWrap = "wrap";
    wrap.innerHTML = `flex-wrap: wrap`;
    container.style.alignContent = "flex-start";
    alignContent.innerHTML = `align-content: flex-start`;
  }
}

function addItem() {
  findElements();
  itemCount++;
  $("#container").append(
    `<div class="flex-item flex-item-${itemCount}">${itemCount}</div>`
  );
}

function removeItem() {
  findElements();
  $(`.flex-item-${itemCount}`).remove();
  itemCount--;
}




window.onload = (event) => {
  if(sessionStorage.getItem('status') != "loggedIn"){
    window.location.href = "../pages/form.html";
  }
}
