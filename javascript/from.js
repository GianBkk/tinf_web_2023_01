import { getUser } from "./users.js";

const form = document.querySelector("form");

form.addEventListener("submit", (event) => {
  event.preventDefault(); // Prevent the form from submitting

  const email = document.querySelector("#email").value;
  const password = document.querySelector("#password").value;

  loginUser(email, password);
});

async function loginUser(email, password) {
  const user = await getUser(email, password);
  
  if (user) {
    sessionStorage.setItem("status", "loggedIn");
    sessionStorage.setItem("username", user.username);
    sessionStorage.setItem("email", user.email);
    sessionStorage.setItem("password", user.password);
    sessionStorage.setItem("agb", user.agb);
    const page = document.getElementById("mainFrom");
    page.innerHTML = /*html*/`
    <div>
      <h3>Welcome back , ${user.username}</h3>
      <a href="./index.html">HomePage</a>
    </div>
    `;
    window.location.href = "index.html";
  } else {
    alert("Incorrect username or password. Please try again.");
  }
}

window.onload = (event) => {
  if(sessionStorage.getItem('status') == "loggedIn"){
    const name = sessionStorage.getItem('username');
    const page = document.getElementById("mainFrom");
    page.innerHTML = html`
    <div>
      <h3>Welcome back , ${name}</h3>
      <a href="./index.html">HomePage</a>
    </div>
    `;
    
  }
}

document.getElementById('logout').addEventListener('click', () => {
  sessionStorage.removeItem('status');
  sessionStorage.removeItem("username");
  sessionStorage.removeItem("email");
  sessionStorage.removeItem("password");
  sessionStorage.removeItem("agb");
  window.location.reload();
  alert("You have been logged out");
});
