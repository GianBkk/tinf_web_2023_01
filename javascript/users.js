const api = `https://61f58cf962f1e300173c41c9.mockapi.io/api/v1/todos/users`;

async function addUser(username, email, password, agb) {
  const newUser = {
    username: username,
    email: email,
    password: password,
    agb: agb
  };
  await fetch(api, {
  method: "POST",
  body: JSON.stringify(newUser),
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }
  });

  sessionStorage.setItem('status','loggedIn');
  return true;
}

async function getUser(email, password) {
  const post = await fetch(api).then((res) => res.json());
  let currentUser = null;

  post.forEach(user => {
    if(user.email === email && user.password === password) {
      currentUser = user;
    }
  });

  return currentUser;
}

async function getAllUsers() {
  return await fetch(api).then((res) => res.json());
}

function logout() {
  sessionStorage.removeItem('status');
  sessionStorage.removeItem("username");
  sessionStorage.removeItem("email");
  sessionStorage.removeItem("password");
  sessionStorage.removeItem("agb");
  window.location.reload();
  alert("You have been logged out");
}
export { addUser, getUser, getAllUsers, logout };
