
window.onload = (event) => {

    if(sessionStorage.getItem('status') == "loggedIn"){
      render(true);
    } else {
        render(false);
    }
  }

function render(isloggedin) {
    const user = sessionStorage.getItem('username');
    document.getElementById('welcome_sign').innerHTML = `Welcome, ${isloggedin == true ? user.toUpperCase() : 'Guest'}`;
    document.getElementById('welcome_btn').innerHTML = `${isloggedin == true ? `<a onclick="logout();">Logout</a>` : `<a href="./form.html">Login</a>`}`;

}


function logout() {
    sessionStorage.removeItem('status');
    sessionStorage.removeItem("username");
    sessionStorage.removeItem("email");
    sessionStorage.removeItem("password");
    window.location.reload();
  }