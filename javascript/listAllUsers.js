import {getAllUsers} from "./users.js";
const container = document.querySelector('.allUsers ul');


document.addEventListener("DOMContentLoaded", function(){
    renderAllUsers();
});

async function renderAllUsers() {
    const users = await getAllUsers();

    
    container.innerHTML = users.map(user => `<li>${user.username + " | " + user.email}</li>`);
        
        

    console.log(users);

}


