var container,search,error404,weatherDetails,weatherBox;

function getElements(){
  container = document.querySelector(".container");
  weatherBox = document.querySelector(".weather-box");
  weatherDetails = document.querySelector(".weather-details");
  error404 = document.querySelector(".not-found");
  search = document.querySelector(".search-box button");
}

function clickButton(){
  fetchData();
}


document.addEventListener("keypress", (e) => {
  if (e.key === "Enter") {
    fetchData();
    return;
  }
});

function fetchData() {
  getElements();
  const APIKey = "7f564a813d0b3aac0db7baedd72a59c1";

  const city = document.querySelector(".search-box input").value;

  if (city === "") return;

  fetch(
    `https://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${APIKey}`
  )
    .then((response) => response.json())
    .then((json) => {
      if (json.cod === "404") {
        // style error page
        container.style.height = "auto";
        weatherBox.style.display = "none";
        weatherDetails.style.display = "none";
        error404.style.display = "block";
        error404.classList.add("fadeIn");
        return;
      }

      error404.style.display = "none";
      error404.classList.remove("fadeIn");

      const image = document.getElementById("weather-icon");
      const temperature = document.getElementById("temperature");
      const description = document.getElementById("description");
      const humidity = document.getElementById("humidity-span");
      const wind = document.getElementById("wind-span");

      switch (json.weather[0].main) {
        // change images based on weather

        case "Clear":
          image.src = "../img/weather/clear.png";
          break;
        case "Rain":
          image.src = "../img/weather/rain.png";
          break;
        case "Snow":
          image.src = "../img/weather/snow.png";
          break;
        case "Clouds":
          image.src = "../img/weather/cloud.png";
          break;
        case "Haze":
          image.src = "../img/weather/haze.png";
          break;
        case "Drizzle":
          image.src = "../img/weather/drizzle.png";
          break;
        case "Sand":
          image.src = "../img/weather/dust.png";
          break;
        default:
          image.src = "";
      }

      temperature.innerHTML = `${parseInt(json.main.temp)}<span>°C</span>`;
      description.innerHTML = `${json.weather[0].description}`;

      humidity.innerHTML = `${json.main.humidity}%`;
      wind.innerHTML = `${parseInt(json.wind.speed)}Km/h`;

      weatherBox.style.display = "";
      weatherDetails.style.display = "";
      weatherBox.classList.add("fadeIn");
      weatherDetails.classList.add("fadeIn");
      container.style.height = "auto";
    });
}

window.onload = (event) => {
  if(sessionStorage.getItem('status') != "loggedIn"){
    window.location.href = "../pages/form.html";
  }
}



