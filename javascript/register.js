import { addUser } from "./users.js";

const form = document.querySelector("form");

form.addEventListener("submit", (event) => {
  event.preventDefault(); // Prevent the form from submitting

  const username = document.querySelector("#username").value;
  const email = document.querySelector("#email").value;
  const password = document.querySelector("#password").value;
  const agbCheck = document.querySelector("#agbCheck").checked;
  
    // Add the new user to the users array
    const isLoggedIn = addUser(username, email, password, agbCheck);
    if (isLoggedIn){
      const page = document.getElementById("mainFrom");
        page.innerHTML = /*html*/`
          <div>
            <h3>Welcome, ${username}</h3>
              ${agbCheck? html`<p>Agreed to AGB</p>` : html`<p>Didn't Agree to AGB</p>`}
            </div>
            `;
    }
 
  
});

window.onload = (event) => {
  if(sessionStorage.getItem('status') == "loggedIn"){
    const name = sessionStorage.getItem('username');
    const agb = sessionStorage.getItem('agb');
    const page = document.getElementById("mainFrom");
        page.innerHTML = /*html*/`
          <div>
            <h3>Welcome, ${name}</h3>
              ${agb? `<p>Agreed to AGB</p>` : `<p>Didn't Agree to AGB</p>`}
            </div>
            `;
  }
}

document.getElementById('logout').addEventListener('click', () => {
  sessionStorage.removeItem('status');
  sessionStorage.removeItem("username");
  sessionStorage.removeItem("email");
  sessionStorage.removeItem("password");
  window.location.reload();
  alert("You have been logged out");
});
