var container;

function getElement(){
  container = document.getElementById("grid");
}

const templates = [
  '"one two four""one three four""five five six"',
  '"one one two""three four four""five six six"',
  '"one two three""four five five""six six six"',
  '"one one one""two three four""six five four"',
  '"six five one""six four one""two three one"',
];
var i = 0;

function changeStyle() {
  getElement();
  set = true;
  do {
    let temp = Math.floor(Math.random() * templates.length);
    if (temp != i) {
      set = false;
      i = temp;
    }
  } while (set);
  container.style.gridTemplateAreas = templates[i];
}



window.onload = (event) => {
  if(sessionStorage.getItem('status') != "loggedIn"){
    window.location.href = "../pages/form.html";
  }
}
